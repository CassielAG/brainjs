# Brain JS

## Entrenamiento:

El algoritmo es capaz de deducir un dígito apartir de los caracteres **#** y **.**. En este caso, la aplicación se orientó a mostrar solamente numeros en formato hexadecimal equivalentes 0-15, es decir, *0-1-2-3-4-5-6-7-8-9-A-B-C-D-E-F*.

Un ejemplo del formato que toma como referencia la IA es el siguiente:
```
const one = character(
  '......#' +
  '......#' +
  '......#' +
  '......#' +
  '......#' +
  '......#' +
  '......#'
);
const two = character(
  '######.' +
  '......#' +
  '......#' +
  '.#####.' +
  '#......' +
  '#......' +
  '.######'
);
const three = character(
  '#######' +
  '......#' +
  '......#' +
  '..#####' +
  '......#' +
  '......#' +
  '#######'
);
```
## Datos de entrada

Dado que no se puede expresar un acomodo tal y como lo solicita la IA, se utilizó varios inputs que simularan el formato requerido.

```
<input type="text" class="form-control" maxlength="7" id="lineone" placeholder="#######">
```

- > Arroyo Reyes José Luis
- > Agüero Gloria José Cassiel
- > 8° "A"

