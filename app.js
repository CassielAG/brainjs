const one = character(
  '......#' +
  '......#' +
  '......#' +
  '......#' +
  '......#' +
  '......#' +
  '......#'
);
const two = character(
  '######.' +
  '......#' +
  '......#' +
  '.#####.' +
  '#......' +
  '#......' +
  '.######'
);
const three = character(
  '#######' +
  '......#' +
  '......#' +
  '..#####' +
  '......#' +
  '......#' +
  '#######'
);
const four = character(
  '#.....#' +
  '#.....#' +
  '#.....#' +
  '#######' +
  '......#' +
  '......#' +
  '......#'
);
const five = character(
  '.######' +
  '#......' +
  '#......' +
  '.#####.' +
  '......#' +
  '......#' +
  '######.'
);
const six = character(
  '.#####.' +
  '#......' +
  '#......' +
  '.#####.' +
  '#.....#' +
  '#.....#' +
  '.#####.'
);
const seven = character(
  '.######' +
  '......#' +
  '......#' +
  '...####' +
  '......#' +
  '......#' +
  '......#'
);
const eight = character(
  '.#####.' +
  '#.....#' +
  '#.....#' +
  '.#####.' +
  '#.....#' +
  '#.....#' +
  '.#####.'
);
const nine = character(
  '.#####.' +
  '#.....#' +
  '#.....#' +
  '.######' +
  '......#' +
  '......#' +
  '.#####.'
);
const a = character(
  '.#####.' +
  '#.....#' +
  '#.....#' +
  '#######' +
  '#.....#' +
  '#.....#' +
  '#.....#'
);
const b = character(
  '######.' +
  '#.....#' +
  '#.....#' +
  '######.' +
  '#.....#' +
  '#.....#' +
  '######.'
);
const c = character(
  '#######' +
  '#......' +
  '#......' +
  '#......' +
  '#......' +
  '#......' +
  '#######'
);
const d = character(
  '#####..' +
  '#.....#' +
  '#.....#' +
  '#.....#' +
  '#.....#' +
  '#.....#' +
  '#####..'
);
const e = character(
  '#######' +
  '#......' +
  '#......' +
  '#####..' +
  '#......' +
  '#......' +
  '#######'
);
const f = character(
  '#######' +
  '#......' +
  '#......' +
  '#####..' +
  '#......' +
  '#......' +
  '#......'
);


/**
 * Learn the letters A through C.
 */
const net = new brain.NeuralNetwork();
net.train([{
    input: one,
    output: {
      one: 1
    }
  },
  {
    input: two,
    output: {
      two: 1
    }
  },
  {
    input: three,
    output: {
      three: 1
    }
  },{
    input: four,
    output: {
      four: 1
    }
  },
  {
    input: five,
    output: {
      five: 1
    }
  },
  {
    input: six,
    output: {
      six: 1
    }
  },
  {
    input: seven,
    output: {
      seven: 1
    }
  },
  {
    input: eight,
    output: {
      eight: 1
    }
  },
  {
    input: nine,
    output: {
      nine: 1
    }
  },
  {
    input: a,
    output: {
      a: 1
    }
  },
  {
    input: b,
    output: {
      b: 1
    }
  },
  {
    input: c,
    output: {
      c: 1
    }
  },
  {
    input: d,
    output: {
      d: 1
    }
  }
], {
  log: detail => console.log(detail)
});

/**
 * Turn the # into 1s and . into 0s. for whole string
 * @param string
 * @returns {Array}
 */
function character(string) {
  return string
    .trim()
    .split('')
    .map(integer);
}

/**
 * Return 0 or 1 for '#'
 * @param character
 * @returns {number}
 */
function integer(character) {
  if ('#' === character) return 1;
  return 0;
}

function cambiar() {
  const result = brain.likely(character(
    document.getElementById("lineone").value +
    document.getElementById("linetwo").value +
    document.getElementById("linethree").value +
    document.getElementById("linefour").value +
    document.getElementById("linefive").value +
    document.getElementById("linesix").value +
    document.getElementById("lineseven").value 
  ), net);

  switch (result) {
    case 'one':
    document.getElementById("num").innerHTML = "1";
    break;
    case 'two':
    document.getElementById("num").innerHTML = '2';
    break;
    case 'three':
    document.getElementById("num").innerHTML = '3';
    break;
    case 'four':
    document.getElementById("num").innerHTML = '4';
    break;
    case 'five':
    document.getElementById("num").innerHTML = '5';
    break;
    case 'six':
    document.getElementById("num").innerHTML = '6';
    break;
    case 'seven':
    document.getElementById("num").innerHTML = '7';
    break;
    case 'eight':
    document.getElementById("num").innerHTML = '8';
    break;
    case 'nine':
    document.getElementById("num").innerHTML = '9';
    break;
    case 'a':
    document.getElementById("num").innerHTML = 'A';
    break;
    case 'b':
    document.getElementById("num").innerHTML = 'B';
    break;
    case 'c':
    document.getElementById("num").innerHTML = 'C';
    break;
    case 'd':
    document.getElementById("num").innerHTML = 'D';
    break;
    case 'e':
    document.getElementById("num").innerHTML = 'E';
    break;
    case 'f':
    document.getElementById("num").innerHTML = 'F';
    break;
    default:
    console.log(result);
    alert("Favor de llenar los campos")
    break;
  }
}